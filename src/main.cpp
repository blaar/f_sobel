#include "blc_core.h"
#include "blc_channel.h"
#include "blc_command.h"
#include "blc_program.h"
#include "blcl.h"

struct blcl_target gpu_target;

#define     WINDOW_SIZE   3

float lFilter_x_gradient[WINDOW_SIZE * WINDOW_SIZE] =
   { -1.0f, 0.0f, 1.0f, -2.0f, 0.0f, 2.0f, -1.0f, 0.0f, 1.0f };
float lFilter_y_gradient[WINDOW_SIZE * WINDOW_SIZE] =
   { -1.0f, -2.0f, -1.0f, 0.0f, 0.0f, 0.0f, 1.f, 2.f, 1.f };

int main(int argc, char *argv[])
{
   blc_channel input, output;
    char *source_code=NULL;
   cl_int status;
   cl_kernel kernel;
   cl_mem cl_input_image, cl_output_image;
   cl_image_format input_image_format, output_image_format;
   int width, height;

   size_t origin[3] =
      { 0, 0, 0 };
    size_t region[3];
    size_t items_nb[2];
    size_t work_group_sizes[] = {1,1};

   char const *output_name, *input_name;

   blc_program_set_description("Apply Sobel filter on the input image");
   blc_program_add_option(&output_name, 'o', "output_channel", "blc_channel", "blc_channel where to put the result", "/sobel_image");
   blc_program_add_parameter(&input_name, "blc_channel", 1, "blc_channel where to get the image.");
   blc_program_init(&argc, &argv, NULL);

   input.open(input_name, BLC_CHANNEL_READ);
   if (input.format != 'Y800' && input.format != 'BA81') EXIT_ON_CHANNEL_ERROR(&input, "The input must be 'Y800' (grey) or 'BA81' (bayer) format");

   width = input.dims[0].length;
   height = input.dims[1].length;

   output.create_or_open(output_name, BLC_CHANNEL_WRITE, 'FL32', 'Y800', 2, width, height);
   output.publish();

   blcl_init(&gpu_target, CL_DEVICE_TYPE_GPU);

   input_image_format.image_channel_order = CL_R;
   input_image_format.image_channel_data_type = CL_UNSIGNED_INT8;
   output_image_format.image_channel_order = CL_R;
   output_image_format.image_channel_data_type = CL_FLOAT;

#ifdef OPENCL_1_2
   cl_image_desc image_desc;
   image_desc.image_type = CL_MEM_OBJECT_IMAGE2D;
   image_desc.image_width = width;
   image_desc.image_height = height;
   image_desc.image_depth = 1;
   image_desc.image_array_size = 1;
   image_desc.image_row_pitch = 0;
   image_desc.image_slice_pitch = 0;
   image_desc.num_mip_levels = 0;
   image_desc.num_samples = 0;
   image_desc.buffer= NULL;
   BLCL_ERROR_CHECK(cl_input_image = clCreateImage( gpu_target.context, CL_MEM_READ_ONLY, &input_image_format, &image_desc,NULL, &status), NULL, status, "allocating input image");
   BLCL_ERROR_CHECK(cl_output_image = clCreateImage( gpu_target.context, CL_MEM_WRITE_ONLY, &output_image_format, &image_desc,NULL, &status), NULL, status, "allocating output image");

   #else
   BLCL_ERROR_CHECK(cl_input_image = clCreateImage2D(gpu_target.context, CL_MEM_READ_ONLY, &input_image_format, width, height, 0, NULL, &status), NULL, status, "allocating input image");
      BLCL_ERROR_CHECK(cl_output_image = clCreateImage2D( gpu_target.context, CL_MEM_WRITE_ONLY, &output_image_format, width, height, 0, NULL, &status), NULL, status, "allocating output image");

   #endif

   asprintf(&source_code, "kernel void sobel(write_only image2d_t output, read_only image2d_t input){\
            int2 coord; //x, y;\n\
            uint4 pixel;\n\
            float4 value;\n\
            float4 grey={1.0f, 0.5f, 0.5f, 0.0f};\n\
            sampler_t my_sampler=CLK_FILTER_NEAREST| CLK_NORMALIZED_COORDS_FALSE |CLK_ADDRESS_NONE;\n\
            coord.x=get_global_id(0); coord.y=get_global_id(1);\n\
            pixel=read_imageui(input,my_sampler, coord);\n\
            value.x=((convert_float(pixel.x))+0.5f)/256.f;\n\
            \n\
            write_imagef(output, coord, value);\n\
            }");
   kernel = BLCL_CREATE_KERNEL_2P(&gpu_target, "sobel", source_code, cl_output_image, cl_input_image);
   FREE(source_code);
    
    items_nb[0]=input.dims[0].length;
    items_nb[1]=input.dims[1].length;
    region[0]=input.dims[0].length;
    region[1]=input.dims[1].length;
    region[2]=1;
    
    BLC_COMMAND_LOOP_NON_STOP(){
        BLCL_CHECK(clEnqueueWriteImage(gpu_target.command_queue, cl_input_image, CL_TRUE,  origin, region,  0,  0, input.data, 0, NULL, NULL ), "Failed to write input array!");
        BLCL_CHECK(clEnqueueNDRangeKernel(gpu_target.command_queue, kernel, 2, NULL, items_nb, work_group_sizes, 0, NULL, NULL), "Failed to execute kernel!");
        BLCL_CHECK(clFinish(gpu_target.command_queue), "finishing");
        BLCL_CHECK(clEnqueueReadImage(gpu_target.command_queue, cl_output_image, CL_TRUE, origin,region, 0, 0, output.data, 0, NULL, NULL ), "Failed to write input array!");
    }


   return 0;
}
